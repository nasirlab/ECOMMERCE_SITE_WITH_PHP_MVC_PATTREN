<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitd65ee5442e9237b93b403ad239a608e6
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/app',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitd65ee5442e9237b93b403ad239a608e6::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitd65ee5442e9237b93b403ad239a608e6::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
